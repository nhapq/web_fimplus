from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '=5d+-7=q7#*ofh=w85yaztw6h-lquf2b1x4pfpcrihw%a*q9is'

ALLOWED_HOSTS = ['*']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS = INSTALLED_APPS + [

    'debug_toolbar',
]

MIDDLEWARE = MIDDLEWARE + [
    # ...
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    # ...
]


INTERNAL_IPS = ("127.0.0.1")
WAGTAIL_CACHE = False

try:
    from .local_settings import *
except ImportError:
    pass
