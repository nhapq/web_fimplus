---
#Fimplus website technical documentation __

---
## Jargoon Defination
- From now on, we use "Title" to indicate a base class of Movie (Phim lẻ) or TVseries (Phim bộ).
- We use "FilmList" to indicate a ribbon from our legacy system.

---
## Scope

**What it is**

- The system includes a content management system (CMS) to:
    + Modify the appearent of the website.
    + Edit the JSON-LD and AMP template.
    + Edit other properties related to SEO.
    + Be able to embed a FilmList in an article (or blog-post) for marketing purpose.
- A website (server-side rendered) to display news, events, introducing our services business.
- A search engine to browse our content.


**And what it isn't**

- This CMS shall not allow editors to alter the media content (name, genre, actor, etc.) Please use the original (legacy) CM for this task.
- This CMS shall not allow editor to alter the structure of FilmList on a page (except the FilmList created specificly for the blogpost,) Please use the original (legacy) CM for this task.
- This API shall not handle the login, nor user's profile, billing.

---
## Installation
1 - 
```cmd
git clone 
```

---  
## Schema
We follow the schema from:
- __[Schema.org](https://schema.org/)__ - To define our schema
- __[Google](https://developers.google.com/actions/media/reference/data-specification/movies-specification?hl=de)__ - To clarify things we do not understand from Schema.org

### Title
``` json
{
  "legacy_meta": {
    "legacy_id": {
      "type": "string",
      "description": "id from old cm"
    },
    "legacy_url": {
      "type": "string",
      "description": "url from old cm"
    },
    "legacy_filmList": {
      "type": "array",
      "description": "a list of legacy ribbon's ids",
      "items": {
        "$ref": "FilmList"
      }
    },
    "legacy_last_modified_date": {
      "type": "string",
      "description": "last modified date on old cm, the legacy cm will take responsible for ensuring this has correct datetime format"
    }
  },
  "content": {
    "id": {
      "type": "string",
      "description": "id of this video"
    },
    "name": {
      "type": "string",
      "description": "English name of this title",
    },
    "alternateName": {
      "type": "string",
      "description": "Vietnamese name of this title"
    },
    "url": {
      "type": "string",
      "description": "url of the item"
    },
    "actor": {
      "type": "array",
      "description": "list of actors",
      "items": {
        "$ref": "Person"
      }
    },
    "director": {
      "type": "array",
      "description": "list of directors",
      "items": {
        "$ref": "Person"
      }
    },
    "countryOfOrigin": {
      "type": "array",
      "description": "list of countries",
      "items": {
        "$ref": "Country"
      }
    },
    "productionCompany": {
      "type": "array",
      "description": "list of organizations",
      "items": {
        "$ref": "Organization"
      }
    },
    "publisher": {
      "type": "array",
      "description": "list of organizations",
      "items": {
        "$ref": "Organization"
      }
    },
    "subtitleLanguage": {
      "type": "array",
      "description": "list of subtitle languages",
      "items": {
        "type": "string"
      }
    },
    "inLanguage": {
      "type": "string",
      "description": "autido language"
    },
    "genre": {
      "type": "array",
      "description": "list of genres, for ex., action, drama",
      "items": {
        "type": "string"
      }
    },
    "description": {
      "type": "string",
      "description": " this is long_description from our legacy cm"
    },
    "disambiguatingDescription": {
      "type": "string",
      "description": "A sub property of description. A short description of the item used to disambiguate from other, similar items, a.k.a, the short_description from legacy cm"
    },
    "aggregateRating": {
      "type": "object",
      "properties": {
        "ratingValue": {
          "type": "number",
          "description": "The rating for the content, can be a decimal number"
        },
        "ratingCount": {
          "type": "integer",
          "description": "The count of total number of ratings.",
          "minimum": 0
        }
      }
    },
    "datePublished": {
      "type": "string",
      "description": "date of first publication, the legacy cm will take responsible for ensuring this has correct format"
    },
    "contentRating": {
      "type": "string",
      "description": "Official rating of a piece of content—for example,'MPAA PG-13'."
    },
    "keywords": {
      "type": "array",
      "description": "Keywords or tags used to describe this content. Multiple entries in a keywords list are typically delimited by commas."
    },
    "identifier": {
      "type" : "object",
      "description" : "External or other ID that unambiguously identifies this entity. Multiple identifiers are allowed. \n
      consume more detail at https://developers.google.com/actions/media/reference/data-specification/watch-actions-common-specification?hl=de#identifier_properties ",
      "properties": {
        "@type" : {
          "type" : "string",
          "description": "Always set to PropertyValue."
        },
        "propertyID" : {
          "type": "string",
          "description" : "one of the following ID types: \n
                          TMS_ROOT_ID: The Gracenote (TMS) Root ID (for example, 15829). \n
                          TMS_ID: The Gracenote (TMS) Variant ID (for example, MV000398520000). \n
                          WIKIDATA_ID: The WikiData ID (for example, Q134773).
                          IMDB_ID: The IMDB ID (for example, tt0109830)."
        },
        "value" : {
          "stype": "string",
          "description": "The value for the specified ID system, (for example, tt0109830)"
      }
    },
    "image": {
      "type": "array",
      "description": "list of images of this title",
      "items": {
        "$ref": "Image"
      }
    },
    "trailer": {
      "type": "array",
      "description": "list of trailers of this title",
      "items": {
        "$ref": "Video"
      }
    },
    "@type" : {
      "type" : "string",
      "description": "This define whether this title is a movie, tvseries or episode. \n
      other sub-class of Title will changed depends on this attribute. For more detail, \n
      please consume https://developers.google.com/actions/media/collect-information/entity-types/identify-the-relationships-tv-show?hl=de#tvseries_tvseason_and_tvepisode_relations \n
      accept: 'Movie', 'TVSeries', 'TVSeason', 'TVEpisode'"
    } 
  }
}
```

### Movie
This schema inherites all properties of Title, with some additionals:
```json
{
  //...properties from Title,
  "content" : {
    // ...
    "duration": {
      "type": "string",
      "description": "duration of this title, the legacy cm will take responsible for ensuring this has correct format"
    },
    "video": {
      "type": "array",
      "description": "list of videos of this title",
      "items": {
        "$ref": "Video"
      }
    },
  }
}
```
### TVSeries
This schema inherites all properties of Title, with some additionals:
```json
{
  //...properties from Title,
  "content" : {
    // ...
    "numberOfSeasons": {
      "type" : "integer",
      "description": "The number of seasons in this series."
    },
    "containsSeason" : {
      "type": "array",
      "description": "a list of TVSeason",
      "items" :{
        "$ref": "TVSeason"
      }
    }
  }
}
```

### TVSeason
This schema inherites all properties of Title, with some additionals:
```json
{ 
   //...properties from Title,
  "content" : {
    // ...
    "seasonNumber" : {
      "type": "integer",
      "description": "Position of the season within an ordered group of seasons."
    },
    "numberOfEpisodes": {
      "type": "integer",
      "description": "The number of episodes in this season or series."
    },
    "partOfSeries": {
      "id": {
        "type": "string",
        "description": "id of its parent TVSeries, can be its url"
      },
      "name" : {
        "type": "string",
        "description": "name of its parent TVSeries"
      }
    },
    "episode" :{
      "type" :"array",
      "description" : "a list of episodes",
      "items" : {
        "$ref": "TVEpisode"
      }
    }

}
```

### TVEpisode
This schema inherites all properties of Title, with some additionals:
``` json
{
  //...properties from Title,
  "content" : {
    // ...
    "duration": {
      "type": "string",
      "description": "duration of this title, the legacy cm will take responsible for ensuring this has correct format"
    },
    "video": {
      "type": "array",
      "description": "list of videos of this episode",
      "items": {
        "$ref": "Video"
      }
    },
    "episodeNumber" : {
      "type" : "integer",
      "description": "Position of the episode within an ordered group of episodes."
    },
    "partOfSeries": {
      "id": {
        "type": "string",
        "description": "id of its parent TVSeries, can be the url"
      },
      "name" : {
        "type": "string",
        "description": "name of its parent TVSeries"
      }
    },

    "partOfSeason": {
      "id": {
        "type": "string",
        "description": "id of its parent TVSeason, can be the url"
      },
      "name" : {
        "type": "string",
        "description": "name of its parent TVSeason"
      }
    },
  }
}
```



### Video
``` json
{
  "id": {
    "type": "string",
    "description": "id of this video"
  },
  "name": {
    "type": "string",
    "description": "name of this video"
  },
  "encodingFormat": {
    "type": "array",
    "description": "format type of video, for ex., hlv, mp4, etc.",
    "items": {
      "type": "string"
    },
  },
  "videoFrameSize": {
    "type": "array",
    "description": "1920 x 1080 pixels, and 1280 x 720, etc",
    "items": {
      "type": "string"
    }
  },
  "url": {
    "type": "string",
    "description": "url to the media file"
  },
  "thumbnail": {
    "type": "array",
    "description": "list of thumbnail images",
    "items": {
      "$ref": "Image"
    }
  },
  "videoFormat": {
    "type": "string",
    "description": """we take this from google: https://developers.google.com/actions/media/reference/data-specification/live-tv/broadcastservice-specification?hl=de \n
    The highest quality definition supported on this broadcast service; for example: \n 
    'SD' for standard-definition, 'HD' for high-definition, '4K' for 4K resolution, '8K' for 8K resolution."""
  }
}
```

### Person
```json
{
  "id": {
    "type": "string",
  },
  "name": {
    "type" : "string",
    "description" : "english name of person",
    "example": "John Doe"
  },
  "image": {
    "type" : "array",
    "description": "list of images of this person",
    "items": {
      "$ref": "Image"
    }
  },
  "sameAs": {
    "type": "string",
    "description": "A URL to a reference web page that can identify the movie; for example, the Wikipedia page of the movie. This must be distinct from the url property.",
    "example": "https://en.wikipedia.org/wiki/John_Doe"
  }
}
```

### Image

```json
{
  "name" : {
    "type" : "string",
    "description": "name of image",
  },
  "url" :{
    "type" : "string",
    "description": "url of image",
  }
}
```
### Country
``` json
{
  "name": {
    "type": "string",
    "description": "English name of country"
  },
  "alternateName": {
    "type" : "string",
    "description": "Vietnamese name of country"
  },
  "addressCountry" : {
    "type": "string",
    "description": "alpha-2 country code.",
    "example": "VN, HK, US, CN"
  }
}
```
### Organization
```json
{
  "name": {
    "type": "string",
    "description": "English name of organization"
  },
  "alternateName": {
    "type" : "string",
    "description": "Vietnamese name of organization"
  },
}
```

---